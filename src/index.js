import React from 'react';
import ReactDOM from 'react-dom';
import AppHeader from "./components/app-header";
import SearchPanel from "./components/search-panel";
import TodoList from "./components/todo-list";

const App = () => {

    const todoDate = [
        {label: 'Drink Coffe', important: false, id: 1 },
        {label: 'Make Awesome', important: true, id: 2 },
        {label: 'Have a lunch', important: false, id: 2 }
    ];
    return (
        <div>
            <AppHeader/>
            <SearchPanel/>
            <TodoList todos={todoDate} />
        </div>
    );
};

    ReactDOM.render(<App />, document.getElementById('root'));